import React from "react";

function Resume() {
  return (
    <div
      className="rn-resume-area rn-section-gap section-separator"
      id="resume"
    >
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="section-title text-center">
              <span className="subtitle">1+ Years of Experience</span>
              <h2 className="title">My Skills</h2>
            </div>
          </div>
        </div>
        <div className="row mt--45">
          <div className="col-lg-12">
            {/* <!-- Start Tab Content Wrapper  --> */}
            <div className="rn-nav-content tab-content" id="myTabContents">
              {/* <!-- Start Single Tab  --> */}
              <div
                className="tab-pane fade show active fade single-tab-area"
                id="professional"
                role="tabpanel"
                aria-labelledby="professional-tab"
              >
                <div className="personal-experience-inner mt--40">
                  <div className="row row--40">
                    {/* <!-- Start Single Progressbar  --> */}
                    <div className="col-lg-6 col-md-6 col-12 mt_sm--60">
                      <div className="progress-wrapper">
                        <div className="content">
                          <span className="subtitle">Features</span>
                          <h4 className="maintitle">Front-End</h4>

                          {/* <!-- Start Single Progress Charts --> */}
                          <div className="progress-charts">
                            <h6 className="heading heading-h6">React JS</h6>
                            <div className="progress">
                              <div
                                className="progress-bar wow fadeInLeft"
                                data-wow-duration="0.9s"
                                data-wow-delay=".7s"
                                role="progressbar"
                                style={{ width: "30%" }}
                                aria-valuenow="85"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              >
                                <span className="percent-label">30%</span>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Single Progress Charts --> */}

                          {/* <!-- Start Single Progress Charts --> */}
                          <div className="progress-charts">
                            <h6 className="heading heading-h6">Vue JS</h6>
                            <div className="progress">
                              <div
                                className="progress-bar wow fadeInLeft"
                                data-wow-duration="0.5s"
                                data-wow-delay=".3s"
                                role="progressbar"
                                style={{ width: "15%" }}
                                aria-valuenow="85"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              >
                                <span className="percent-label">15%</span>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Single Progress Charts --> */}

                          {/* <!-- Start Single Progress Charts --> */}
                          <div className="progress-charts">
                            <h6 className="heading heading-h6">Angular JS</h6>
                            <div className="progress">
                              <div
                                className="progress-bar wow fadeInLeft"
                                data-wow-duration="0.6s"
                                data-wow-delay=".4s"
                                role="progressbar"
                                style={{ width: "30%" }}
                                aria-valuenow="85"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              >
                                <span className="percent-label">30%</span>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Single Progress Charts --> */}

                          {/* <!-- Start Single Progress Charts --> */}
                          <div className="progress-charts">
                            <h6 className="heading heading-h6">Ember JS</h6>
                            <div className="progress">
                              <div
                                className="progress-bar wow fadeInLeft"
                                data-wow-duration="0.8s"
                                data-wow-delay=".6s"
                                role="progressbar"
                                style={{ width: "25%" }}
                                aria-valuenow="85"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              >
                                <span className="percent-label">25%</span>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Single Progress Charts --> */}

                          {/* <!-- Start Single Progress Charts --> */}
                          <div className="progress-charts">
                            <h6 className="heading heading-h6">Polymer JS</h6>
                            <div className="progress">
                              <div
                                className="progress-bar wow fadeInLeft"
                                data-wow-duration="0.8s"
                                data-wow-delay=".6s"
                                role="progressbar"
                                style={{ width: "25%" }}
                                aria-valuenow="85"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              >
                                <span className="percent-label">25%</span>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Single Progress Charts --> */}
                        </div>
                      </div>
                    </div>
                    {/* <!-- End Single Progressbar  --> */}

                    {/* <!-- Start Single Progressbar  --> */}
                    <div className="col-lg-6 col-md-6 col-12">
                      <div className="progress-wrapper">
                        <div className="content">
                          <span className="subtitle">Features</span>
                          <h4 className="maintitle">Back-End</h4>
                          {/* <!-- Start Single Progress Charts --> */}
                          <div className="progress-charts">
                            <h6 className="heading heading-h6">Express</h6>
                            <div className="progress">
                              <div
                                className="progress-bar wow fadeInLeft"
                                data-wow-duration="0.5s"
                                data-wow-delay=".3s"
                                role="progressbar"
                                style={{ width: "5%" }}
                                aria-valuenow="5"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              >
                                <span className="percent-label">5%</span>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Single Progress Charts --> */}

                          {/* <!-- Start Single Progress Charts --> */}
                          <div className="progress-charts">
                            <h6 className="heading heading-h6">Next</h6>
                            <div className="progress">
                              <div
                                className="progress-bar wow fadeInLeft"
                                data-wow-duration="0.6s"
                                data-wow-delay=".4s"
                                role="progressbar"
                                style={{ width: "9%" }}
                                aria-valuenow="9"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              >
                                <span className="percent-label">9%</span>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Single Progress Charts --> */}

                          {/* <!-- Start Single Progress Charts --> */}
                          <div className="progress-charts">
                            <h6 className="heading heading-h6">Meteor</h6>
                            <div className="progress">
                              <div
                                className="progress-bar wow fadeInLeft"
                                data-wow-duration="0.7s"
                                data-wow-delay=".5s"
                                role="progressbar"
                                style={{ width: "5%" }}
                                aria-valuenow="5"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              >
                                <span className="percent-label">5%</span>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Single Progress Charts --> */}

                          {/* <!-- Start Single Progress Charts --> */}
                          <div className="progress-charts">
                            <h6 className="heading heading-h6">Koa</h6>
                            <div className="progress">
                              <div
                                className="progress-bar wow fadeInLeft"
                                data-wow-duration="0.8s"
                                data-wow-delay=".6s"
                                role="progressbar"
                                style={{ width: "3%" }}
                                aria-valuenow="3"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              >
                                <span className="percent-label">3%</span>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Single Progress Charts --> */}

                          {/* <!-- Start Single Progress Charts --> */}
                          <div className="progress-charts">
                            <h6 className="heading heading-h6">Sails</h6>
                            <div className="progress">
                              <div
                                className="progress-bar wow fadeInLeft"
                                data-wow-duration="0.8s"
                                data-wow-delay=".6s"
                                role="progressbar"
                                style={{ width: "1%" }}
                                aria-valuenow="1"
                                aria-valuemin="0"
                                aria-valuemax="100"
                              >
                                <span className="percent-label">1%</span>
                              </div>
                            </div>
                          </div>
                          {/* <!-- End Single Progress Charts --> */}
                        </div>
                      </div>
                    </div>
                    {/* <!-- End Single Progressbar  --> */}
                  </div>
                </div>
              </div>
              {/* <!-- End Single Tab  --> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Resume;
